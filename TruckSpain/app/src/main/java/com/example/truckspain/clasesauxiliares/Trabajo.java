package com.example.truckspain.clasesauxiliares;

public class Trabajo {
    private String id_trabajo;
    private String nombre;
    private String fecha_realizar;
    private String estado;
    private String email;
    private String nifempresa;
    private String precio;
    private String peso;
    private String lugar_carga;
    private String fecha_inicio;
    private String fecha_final;
    private String ubicacion_inicio;
    private String ubicacion_final;
    private String nombre_responsable;
    private String matricula;
    private String observacion;
    private String obra;


    public Trabajo(String id_trabajo, String nombre, String fecha_realizar, String estado, String email, String nifempresa, String precio, String peso, String lugar_carga, String fecha_inicio, String fecha_final, String ubicacion_inicio, String ubicacion_final, String nombre_responsable, String matricula, String observacion, String obra) {
        this.id_trabajo = id_trabajo;
        this.nombre = nombre;
        this.fecha_realizar = fecha_realizar;
        this.estado = estado;
        this.email = email;
        this.nifempresa = nifempresa;
        this.precio = precio;
        this.peso = peso;
        this.lugar_carga = lugar_carga;
        this.fecha_inicio = fecha_inicio;
        this.fecha_final = fecha_final;
        this.ubicacion_inicio = ubicacion_inicio;
        this.ubicacion_final = ubicacion_final;
        this.nombre_responsable = nombre_responsable;
        this.matricula = matricula;
        this.observacion = observacion;
        this.obra = obra;
    }

    public String getId_trabajo() {
        return id_trabajo;
    }

    public void setId_trabajo(String id_trabajo) {
        this.id_trabajo = id_trabajo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha_realizar() {
        return fecha_realizar;
    }

    public void setFecha_realizar(String fecha_realizar) {
        this.fecha_realizar = fecha_realizar;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNifempresa() {
        return nifempresa;
    }

    public void setNifempresa(String nifempresa) {
        this.nifempresa = nifempresa;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getLugar_carga() {
        return lugar_carga;
    }

    public void setLugar_carga(String lugar_carga) {
        this.lugar_carga = lugar_carga;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_final() {
        return fecha_final;
    }

    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }

    public String getUbicacion_inicio() {
        return ubicacion_inicio;
    }

    public void setUbicacion_inicio(String ubicacion_inicio) {
        this.ubicacion_inicio = ubicacion_inicio;
    }

    public String getUbicacion_final() {
        return ubicacion_final;
    }

    public void setUbicacion_final(String ubicacion_final) {
        this.ubicacion_final = ubicacion_final;
    }

    public String getNombre_responsable() {
        return nombre_responsable;
    }

    public void setNombre_responsable(String nombre_responsable) {
        this.nombre_responsable = nombre_responsable;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObra() {
        return obra;
    }

    public void setObra(String obra) {
        this.obra = obra;
    }
}
