package com.example.truckspain.ventanas.ui.CambiarContrasena;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ApiREST {
    private InputStream is;
    public InputStream inputStream(String txt){
        try{
            URL url = new URL(txt);
            HttpURLConnection urlconn = (HttpURLConnection) url.openConnection();
            urlconn.connect();
            is = urlconn.getInputStream();
        }catch (Exception e){
            e.printStackTrace();
        }
        return is;
    }
}
