package com.example.truckspain.ventanas.ui.ConfigurarFirma;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.truckspain.R;

import java.io.File;
import java.io.FileOutputStream;

public class ConfigurarFirmaFragment extends Fragment {

    private Button Btn_FIRMAUSUARIO;
    private Button mGetSign;
    private Button BorrarFirmaButton;
    private ImageView FirmaUsuarioImageView;
    private String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/Truck/user/";
    private String StoredPath = DIRECTORY + "user_sign" + ".png";
    private LinearLayout linearLayout;
    private Dialog dialog;
    private Signature signature;
    private View view;

    @SuppressLint("InflateParams")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);

        FirmaUsuarioImageView = root.findViewById(R.id.FirmaUsuarioImageView);
        Btn_FIRMAUSUARIO = root.findViewById(R.id.Btn_FIRMAUSUARIO);
        BorrarFirmaButton = root.findViewById(R.id.BorrarFirmaButton);

        File Directory = new File(DIRECTORY);
        if (!Directory.exists()) {
            Directory.mkdir();
        }

        File file = new File(StoredPath);
        if(file.exists()){
            FirmaUsuarioImageView.setImageBitmap(BitmapFactory.decodeFile(StoredPath));
            Btn_FIRMAUSUARIO.setVisibility(View.INVISIBLE);
            BorrarFirmaButton.setVisibility(View.VISIBLE);
        }

        Button GuardarCambiosButton = root.findViewById(R.id.GuardarCambiosButton);
        GuardarCambiosButton.setOnClickListener(v -> {
            View alertLayout = inflater.inflate(R.layout.layout_alert_configurarfirma, null);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(requireActivity());
            alertDialog.setCancelable(false)
                    .setPositiveButton("Aceptar", (dialogInterface, i) -> dialogInterface.cancel());
            AlertDialog alertDialogLayout = alertDialog.create();
            alertDialogLayout.setView(alertLayout);
            alertDialogLayout.show();
        });

        dialog = new Dialog(requireActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_configurarfirma_firmar);
        dialog.setCancelable(true);
        Button Btn_FIRMAUSUARIO = root.findViewById(R.id.Btn_FIRMAUSUARIO);
        Btn_FIRMAUSUARIO.setOnClickListener(v -> dialog_action());

        Button BorrarFirmaButton = root.findViewById(R.id.BorrarFirmaButton);
        BorrarFirmaButton.setOnClickListener(v -> {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(requireActivity());
            alertDialog.setCancelable(false)
                    .setPositiveButton("Aceptar", (dialogInterface, i) -> aceptarBorrarFirma())
                    .setNegativeButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
            AlertDialog alertDialogLayout = alertDialog.create();
            alertDialogLayout.setTitle("¿Quieres borrar tú firma?");
            alertDialogLayout.setMessage("Recuerda: Si no tienes establecida una firma, no podrás realizar trabajos.");
            alertDialogLayout.show();
        });

        return root;
    }

    private void aceptarBorrarFirma() {
        FirmaUsuarioImageView.setImageResource(R.color.white);
        Btn_FIRMAUSUARIO.setVisibility(View.VISIBLE);
        BorrarFirmaButton.setVisibility(View.INVISIBLE);
    }

    private void dialog_action() {
        linearLayout = dialog.findViewById(R.id.linearLayout);
        signature = new Signature(requireActivity().getApplicationContext(), null);
        signature.setBackgroundColor(Color.WHITE);
        // Dynamically generating Layout through java code
        linearLayout.addView(signature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        Button mClear = dialog.findViewById(R.id.clear);
        mGetSign = dialog.findViewById(R.id.getsign);
        mGetSign.setEnabled(false);
        view = linearLayout;

        mClear.setOnClickListener(v -> {
            //BORRAR
            signature.clear();
            mGetSign.setEnabled(false);
        });
        mGetSign.setOnClickListener(v -> {
            //GUARDAR
            FirmaUsuarioImageView.setVisibility(View.VISIBLE);
            Btn_FIRMAUSUARIO.setVisibility(View.INVISIBLE);
            BorrarFirmaButton.setVisibility(View.VISIBLE);

            view.setDrawingCacheEnabled(true);
            signature.save(view, StoredPath);
            dialog.dismiss();
            // Calling the same class
            FirmaUsuarioImageView.setImageBitmap(BitmapFactory.decodeFile(""+StoredPath));


        });
        dialog.show();
    }

    public class Signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        @SuppressLint("WrongThread")
        public void save(View v, String StoredPath) {

            Bitmap bitmap = Bitmap.createBitmap(linearLayout.getWidth(), linearLayout.getHeight(), Bitmap.Config.RGB_565);

            Canvas canvas = new Canvas(bitmap);
            try {
                // Output the file
                FileOutputStream mFileOutStream = new FileOutputStream(StoredPath);
                v.draw(canvas);

                bitmap.compress(Bitmap.CompressFormat.PNG, 70, mFileOutStream);
                // Convert the output file to Image such as .png
                mFileOutStream.flush();
                mFileOutStream.close();
            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }

        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:
                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;
                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {
            Log.v("log_tag", string);
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
}
