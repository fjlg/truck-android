package com.example.truckspain.entidades;

public class TrabajosDatos {
    private String email;

    public TrabajosDatos(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
