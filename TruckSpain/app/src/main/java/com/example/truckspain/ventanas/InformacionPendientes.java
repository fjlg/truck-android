package com.example.truckspain.ventanas;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.truckspain.R;
import com.example.truckspain.api.TruckApiService;
import com.example.truckspain.clasesauxiliares.Empresa;
import com.example.truckspain.entidades.EmpresasDatos;
import com.example.truckspain.entidades.TrabajosUpdateDatos;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class InformacionPendientes extends AppCompatActivity {

    private LocationManager locationManager;
    private ProgressDialog ProgressDialog;
    private String id_trabajo, nombre, estado, nifempresa, lugar_carga, obra, fecha_realizar, email, peso, observacion;
    private String nombre_empresa, direccion, provincia, poblacion, codigo_postal, telefono, email_empresa;
    ExtendedFloatingActionButton EmpezarTrabajoExtendedFloatingActionButton, TerminarTrabajoExtendedFloatingActionButton;
    private TextView rellenarTelefonoTextView;
    private TextView rellenarObraTextView;

    @SuppressLint({"SourceLockedOrientationActivity", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_informacionpendientes);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        EmpezarTrabajoExtendedFloatingActionButton = findViewById(R.id.EmpezarTrabajoExtendedFloatingActionButton);
        TerminarTrabajoExtendedFloatingActionButton = findViewById(R.id.TerminarTrabajoExtendedFloatingActionButton);

        Intent intent = getIntent();
        id_trabajo = intent.getStringExtra("id_trabajo");                   nombre = intent.getStringExtra("nombre");
        fecha_realizar = intent.getStringExtra("fecha_realizar");    estado = intent.getStringExtra("estado");
        email = intent.getStringExtra("email");                      nifempresa = intent.getStringExtra("nifempresa");
        peso = intent.getStringExtra("peso");                        lugar_carga = intent.getStringExtra("lugar_carga");
        observacion = intent.getStringExtra("observacion");          obra = intent.getStringExtra("obra");

        TextView rellenarNombreTextView = findViewById(R.id.RellenarNombreTextView);            rellenarNombreTextView.setText(nombre);
        TextView rellenarDireccionTextView = findViewById(R.id.RellenarDireccionTextView);      rellenarDireccionTextView.setText(lugar_carga);
        rellenarObraTextView = findViewById(R.id.RellenarObraTextView);                rellenarObraTextView.setText(obra);
        TextView rellenarFechaMesTextView = findViewById(R.id.RellenarFechaMesTextView);        rellenarFechaMesTextView.setText(MesFecha(Objects.requireNonNull(fecha_realizar)));
        TextView rellenarFechaDiaTextView = findViewById(R.id.RellenarFechaDiaTextView);        rellenarFechaDiaTextView.setText(fecha_realizar.substring(8,10));
        TextView rellenarFechaAnioTextView = findViewById(R.id.RellenarFechaAnioTextView);      rellenarFechaAnioTextView.setText(fecha_realizar.substring(2,4));
        TextView rellenarPesoTextView = findViewById(R.id.RellenarPesoTextView);                rellenarPesoTextView.setText(peso+" t");
        TextView rellenarObservacionTextView = findViewById(R.id.RellenarObservacionTextView);  rellenarObservacionTextView.setText(observacion);

        assert estado != null;
        if(estado.equals("En Curso")) {
            EmpezarTrabajoExtendedFloatingActionButton.setEnabled(false);
            EmpezarTrabajoExtendedFloatingActionButton.setElevation(10);
        }else{
            TerminarTrabajoExtendedFloatingActionButton.setEnabled(false);
            TerminarTrabajoExtendedFloatingActionButton.setElevation(10);
        }

        ObtenerDatos(nifempresa);
        ComprobarFirmaUsuario();
    }

    private void ComprobarFirmaUsuario() {
        String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/Truck/user/";
        String StoredPath2 = DIRECTORY + "user_sign" + ".png";
        File archivo = new File(StoredPath2);
        if(!archivo.exists()){
            AlertDialog.Builder alerta = new AlertDialog.Builder(InformacionPendientes.this);
            alerta  .setCancelable(false)
                    .setPositiveButton("Aceptar", (dialogInterface, i) -> finish());
            AlertDialog titulo = alerta.create();
            titulo.setTitle("Configurar Firma");
            titulo.setMessage("Si no tienes establecida una firma, no podrás realizar trabajos.");
            titulo.show();
        }
    }

    public void volverActivityAnterior(View view) {
        Intent intent = new Intent(InformacionPendientes.this, BandejaEntrada.class);
        startActivity(intent);
        finish();
    }

    private void ObtenerDatos(String nif_empresa) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            ProgressDialog = new ProgressDialog(this);
            ProgressDialog.setTitle("Cargando");
            ProgressDialog.setMessage("Recogiendo datos...");
            ProgressDialog.setCancelable(false);
            ProgressDialog.show();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://truckspain.000webhostapp.com/apitruck/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            TruckApiService service = retrofit.create(TruckApiService.class);

            Call<List<Empresa>> truckRespuestaCallEmpresa = service.obtenerEmpresa(new EmpresasDatos(nif_empresa));
            truckRespuestaCallEmpresa.enqueue(new Callback<List<Empresa>>() {
                @Override
                public void onResponse(@NonNull Call<List<Empresa>> call, @NonNull Response<List<Empresa>> response) {
                    if (response.isSuccessful()) {
                        List<Empresa> empresa = response.body();
                        assert empresa != null;

                        TextView rellenarEmpresaTextView = findViewById(R.id.RellenarEmpresaTextView);       rellenarEmpresaTextView.setText(empresa.get(0).getNombre_empresa());
                        rellenarTelefonoTextView = findViewById(R.id.RellenarTelefonoTextView);     rellenarTelefonoTextView.setText(empresa.get(0).getTelefono());

                        nombre_empresa = empresa.get(0).getNombre_empresa();
                        direccion = empresa.get(0).getDireccion();
                        provincia = empresa.get(0).getProvincia();
                        poblacion = empresa.get(0).getPoblacion();
                        codigo_postal = empresa.get(0).getCodigo_postal();
                        telefono = empresa.get(0).getTelefono();
                        email_empresa = empresa.get(0).getEmail_empresa();

                        ProgressDialog.dismiss();


                    } else {
                        Log.e(TAG, " onResponse: " + response.errorBody());
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                        ProgressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<List<Empresa>> call, @NonNull Throwable t) {
                    Log.e(TAG, " onFailure: " + t.getMessage());
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    ProgressDialog.dismiss();
                }
            });

        } else { sinConexion(); }
    }

    @SuppressLint("MissingPermission")
    public void empezarTrabajo(View view) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setCancelable(false)
                    .setPositiveButton("Aceptar", (dialogInterface, i) -> {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 20 * 1000, 10, locationListenerNetwork);
                        ProgressDialog = new ProgressDialog(this);
                        ProgressDialog.setTitle("Cargando");
                        ProgressDialog.setMessage("Aceptando el Trabajo...");
                        ProgressDialog.setCancelable(false);
                        ProgressDialog.show();
                    })
                    .setNegativeButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
            AlertDialog alertDialogLayout = alertDialog.create();
            alertDialogLayout.setTitle("¿Estás listo para empezar?");
            alertDialogLayout.setMessage("Una vez que hayas iniciado el trabajo no podrás volver atrás.");
            alertDialogLayout.show();

        } else { sinConexion(); }
    }

    public String MesFecha(String fecha_realizar){
        String mes = fecha_realizar.substring(5,7);
        switch (mes) {
            case "01": return "ENE";
            case "02": return "FEB";
            case "03": return "MAR";
            case "04": return "ABR";
            case "05": return "MAY";
            case "06": return "JUN";
            case "07": return "JUL";
            case "08": return "AGO";
            case "09": return "SEP";
            case "10": return "OCT";
            case "11": return "NOV";
            case "12": return"DIC";
            default: return "ERR";
        }
    }

    private final LocationListener locationListenerNetwork = new LocationListener() {

        public void onLocationChanged(Location location) {
            double longitudeNetwork = location.getLongitude();
            double latitudeNetwork = location.getLatitude();

            runOnUiThread(() -> {
                String Latitude = latitudeNetwork + "";
                String Longitude = longitudeNetwork + "";
                String ubicacionInicio = Latitude + ","+ Longitude;
                String hora_aceptado = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());

                EmpezarTrabajoExtendedFloatingActionButton.setEnabled(false); EmpezarTrabajoExtendedFloatingActionButton.setElevation(10);
                TerminarTrabajoExtendedFloatingActionButton.setEnabled(true);

                updateTrabajo2(id_trabajo, hora_aceptado, ubicacionInicio);
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {}
        @Override
        public void onProviderEnabled(String s) {}
        @Override
        public void onProviderDisabled(String s) {}
    };

    @SuppressLint("MissingPermission")
    public void terminarTrabajo(View view) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setCancelable(false)
                    .setPositiveButton("Aceptar", (dialogInterface, i) -> {

                        Intent intent = new Intent(InformacionPendientes.this, InformacionAlbaran.class);
                        intent.putExtra("id_trabajo", id_trabajo);
                        intent.putExtra("nombre", nombre);
                        intent.putExtra("fecha_realizar", fecha_realizar);
                        intent.putExtra("estado", estado);
                        intent.putExtra("email", email);
                        intent.putExtra("nifempresa", nifempresa);
                        intent.putExtra("peso", peso);
                        intent.putExtra("lugar_carga", lugar_carga);
                        intent.putExtra("observacion", observacion);
                        intent.putExtra("obra", obra);

                        intent.putExtra("nombre_empresa",nombre_empresa);
                        intent.putExtra("direccion",direccion);
                        intent.putExtra("provincia",provincia);
                        intent.putExtra("poblacion",poblacion);
                        intent.putExtra("codigo_postal",codigo_postal);
                        intent.putExtra("telefono",telefono);
                        intent.putExtra("email_empresa",email_empresa);

                        startActivity(intent);
                    })
                    .setNegativeButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
            AlertDialog alertDialogLayout = alertDialog.create();
            alertDialogLayout.setTitle("¿Quieres finalizar el trabajo?");
            alertDialogLayout.setMessage("Una vez que hayas finalizado el trabajo no podrás volver atrás.");
            alertDialogLayout.show();
        } else { sinConexion(); }
    }

    public void buttonLlaveInglesa(View view) {
        String phoneNo = "112";
        String dial = "tel:" + phoneNo;
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
    }

    public void buttonTelefono(View view) {
        String phoneNo = String.valueOf(rellenarTelefonoTextView.getText());
        String dial = "tel:" + phoneNo;
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
    }

    public void buttonLocalizacion(View view) {
        String map = "http://maps.google.com/maps?q=" + rellenarObraTextView.getText();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
        startActivity(intent);
    }

    private void updateTrabajo2(String id_trabajo, String hora_aceptado, String ubicacionInicio) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {

            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    //.sslSocketFactory(new TLSSocketFactory(),trustManager)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .readTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)
                    //.sslSocketFactory(sslSocketFactory, trustManager)
                    .followRedirects(false)
                    .followSslRedirects(false)
                    .retryOnConnectionFailure(false)
                    .cache(null)//new Cache(sContext.getCacheDir(),10*1024*1024)
                    .build();

            Retrofit retrofit2 = new Retrofit.Builder()
                    .baseUrl("https://truckspain.000webhostapp.com/apitruck/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            TruckApiService service = retrofit2.create(TruckApiService.class);
            Call<String> truckRespuestaCallUpdateTrabajos = service.updateTrabajo(new TrabajosUpdateDatos(id_trabajo, hora_aceptado, ubicacionInicio, "","", ""));
            truckRespuestaCallUpdateTrabajos.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    if (response.isSuccessful()) {
                        String trabajo = response.body();
                        assert trabajo != null;
                        ProgressDialog.dismiss();
                        truckRespuestaCallUpdateTrabajos.cancel();
                    } else {
                        Log.e(TAG, " onResponse: " + response.errorBody());
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                        ProgressDialog.dismiss();
                        truckRespuestaCallUpdateTrabajos.cancel();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    Log.e(TAG, " onFailure: " + t.getMessage());
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    ProgressDialog.dismiss();
                    truckRespuestaCallUpdateTrabajos.cancel();
                }
            });

        } else { sinConexion(); }
    }

    public void sinConexion(){
        AlertDialog.Builder alerta = new AlertDialog.Builder(InformacionPendientes.this);
        alerta  .setCancelable(false).setPositiveButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
        AlertDialog titulo = alerta.create();
        titulo.setTitle("No tiene conexión...");
        titulo.show();
    }
}
