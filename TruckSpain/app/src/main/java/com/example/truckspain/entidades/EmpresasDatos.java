package com.example.truckspain.entidades;

public class EmpresasDatos {
    private String nif_empresa;

    public EmpresasDatos(String nif_empresa) {
        this.nif_empresa = nif_empresa;
    }

    /**
     *
     * @return
     * The nif_empresa
     */
    public String getEmail() {
        return nif_empresa;
    }

    /**
     *
     * @param nif_empresa
     * The nif_empresa
     */
    public void setEmail(String nif_empresa) {
        this.nif_empresa = nif_empresa;
    }

}
