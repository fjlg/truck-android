package com.example.truckspain.clasesauxiliares;

import java.util.ArrayList;

/**
 * Created by Daniel Alvarez on 28/7/16.
 * Copyright © 2016 Alvarez.tech. All rights reserved.
 */
public class TruckRespuesta {

    private ArrayList<Usuario> usuario;
    private String status;

    public String getLogin() {
        return status;
    }

    public void setLogin(String status) {
        this.status = status;
    }

    public ArrayList<Usuario> getResults() {
        return usuario;
    }

    public void setResults(ArrayList<Usuario> results) { this.usuario = results; }

}
