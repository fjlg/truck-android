package com.example.truckspain.ventanas;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.truckspain.R;
import com.google.android.material.navigation.NavigationView;

public class BandejaEntrada extends AppCompatActivity{

    private AppBarConfiguration appBarConfiguration;
    private  String email_usuario;

    @SuppressLint({"SourceLockedOrientationActivity", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_bandejaentrada);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Identificamos si el dispositivo tiene conexion a Internet
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();


        if (networkInfo != null && networkInfo.isConnected()) {
            // Si hay conexión a Internet en este momento
            SharedPreferences preferences=getSharedPreferences("credenciales", Context.MODE_PRIVATE);
            email_usuario = preferences.getString("email","No existe");
        } else {
            // No hay conexión a Internet en este momento
            SharedPreferences preferences=getSharedPreferences("credenciales", Context.MODE_PRIVATE);
            email_usuario = preferences.getString("email","No existe");
            sinConexion();
        }

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        // Pasar cada ID de menú como un conjunto de ID porque cada
        //menú debe considerarse como destinos de nivel superior.
        appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_cerrarsesion)
                .setDrawerLayout(drawerLayout)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        TextView NombreUsuarioTextView = headerView.findViewById(R.id.NombreUsuarioTextView);
        TextView EmailUsuarioTextView = headerView.findViewById(R.id.EmailUsuarioTextView);
        EmailUsuarioTextView.setText("");
        NombreUsuarioTextView.setText("Truck. Spain S.L.");
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    public void sinConexion(){
        AlertDialog.Builder alerta = new AlertDialog.Builder(BandejaEntrada.this);
        alerta  .setCancelable(false).setPositiveButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
        AlertDialog titulo = alerta.create();
        titulo.setTitle("No tiene conexión...");
        titulo.show();
    }

    public String getEmailUsuario() {
        return email_usuario;
    }
}
