package com.example.truckspain.entidades;

public class UsuariosUpdateDatos {
    private String email;
    private String pass;

    public UsuariosUpdateDatos(String email, String pass) {
        this.email = email;
        this.pass = pass;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
            return email;
        }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
            this.email = email;
        }

    /**
     *
     * @return
     * The pass
     */
    public String getPass() {
        return pass;
    }

    /**
     *
     * @param pass
     * The pass
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

}
