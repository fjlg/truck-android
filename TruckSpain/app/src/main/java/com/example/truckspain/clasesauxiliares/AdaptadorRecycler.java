package com.example.truckspain.clasesauxiliares;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.truckspain.R;

import java.util.ArrayList;

public class AdaptadorRecycler extends RecyclerView.Adapter<AdaptadorRecycler.ViewHolderDatos> implements View.OnClickListener{

    private ArrayList<Trabajo> listTrabajo;
    private View.OnClickListener listener;

    public AdaptadorRecycler(ArrayList<Trabajo> listTrabajo) {
        this.listTrabajo = listTrabajo;
    }

    @NonNull
    @Override //Nos enlaza el adaptador con el list.xml
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_lista,null,false);

        view.setOnClickListener(this);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos holder, int position) {
        holder.nombre.setText(listTrabajo.get(position).getNombre());
        holder.destino.setText(listTrabajo.get(position).getLugar_carga());
        holder.fecha.setText(listTrabajo.get(position).getFecha_realizar());
        holder.id.setText(listTrabajo.get(position).getId_trabajo());
        holder.estado.setText(listTrabajo.get(position).getEstado());
    }

    @Override
    public int getItemCount() {
        return listTrabajo.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View view) {
        if(listener!=null){
            listener.onClick(view);
        }
    }

    static class ViewHolderDatos extends RecyclerView.ViewHolder {

        TextView nombre, destino, fecha, id, estado;

        ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.NombreTrabajoTextView);
            destino = itemView.findViewById(R.id.PoblacionTrabajoTextView);
            fecha = itemView.findViewById(R.id.FechaTrabajoTextView);
            id = itemView.findViewById(R.id.IdTrabajoTextView);
            estado = itemView.findViewById(R.id.EstadoTrabajoTextView);
        }
    }
}
