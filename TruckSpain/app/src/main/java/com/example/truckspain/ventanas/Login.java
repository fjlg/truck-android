package com.example.truckspain.ventanas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.appcompat.app.AlertDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.example.truckspain.api.TruckApiService;
import com.example.truckspain.clasesauxiliares.Token;
import com.example.truckspain.entidades.LoginDatos;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.Objects;

import com.example.truckspain.R;

public class Login extends AppCompatActivity implements View.OnClickListener {

    private Retrofit retrofit;
    private ProgressDialog ProgressDialog;
    private static final String TAG = "LOG";
    private TextInputEditText UsuarioTextInputEditText, PasswordTextInputEditText;
    private TextInputLayout UsuarioTextInputLayout, PasswordTextInputLayout;
    private String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/Truck";
    private String FOLDER00 = Environment.getExternalStorageDirectory().getPath() + "/Truck"+"/user";
    private String FOLDER01 = Environment.getExternalStorageDirectory().getPath() + "/Truck/sign";
    private String FOLDER02 = Environment.getExternalStorageDirectory().getPath() + "/Truck/PDF";

    @SuppressLint("StaticFieldLeak")
    public static RadioButton MantenerSesionRadioButton;
    private boolean mantenerSesionIniciada;
    public static String STRING_PREFERNECES = "com.example.truck";
    public static String PREFERENCE_STATE_BTN_SESION = "state.btn.sesion";

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        if(sesionAbierta()) {
            Intent intent = new Intent(Login.this, BandejaEntrada.class);
            startActivity(intent);
            finish();
        }

        File file = new File(DIRECTORY);
        if (!file.exists()) {
            file.mkdir();
        }

        File folder00 = new File(FOLDER00);
        File folder01 = new File(FOLDER01);
        File folder02 = new File(FOLDER02);
        if (!folder00.exists()) {
            folder00.mkdir();
        }
        if (!folder01.exists()) {
            folder01.mkdir();
        }
        if (!folder02.exists()) {
            folder02.mkdir();
        }

        UsuarioTextInputEditText = findViewById(R.id.UsuarioTextInputEditText);
        PasswordTextInputEditText = findViewById(R.id.PasswordTextInputEditText);
        UsuarioTextInputLayout = findViewById(R.id.UsuarioTextInputLayout);
        PasswordTextInputLayout = findViewById(R.id.PasswordTextInputLayout);

        Button iniciarSesionButton = findViewById(R.id.IniciarSesionButton);
        iniciarSesionButton.setOnClickListener(this);

        MantenerSesionRadioButton = findViewById(R.id.MantenerSesionRadioButton);
        mantenerSesionIniciada = MantenerSesionRadioButton.isChecked();

        MantenerSesionRadioButton.setOnClickListener(view -> {
            if(mantenerSesionIniciada){MantenerSesionRadioButton.setChecked(false);}
            mantenerSesionIniciada = MantenerSesionRadioButton.isChecked();
        });

        retrofit = new Retrofit.Builder()
                .baseUrl("https://truckspain.000webhostapp.com/apitruck/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void guardarEstadoSesion() {
        SharedPreferences preferences = getSharedPreferences(STRING_PREFERNECES,MODE_PRIVATE);
        preferences.edit().putBoolean(PREFERENCE_STATE_BTN_SESION,MantenerSesionRadioButton.isChecked()).apply();
    }

    public boolean sesionAbierta() {
        SharedPreferences preferences = getSharedPreferences(STRING_PREFERNECES,MODE_PRIVATE);
        return preferences.getBoolean(PREFERENCE_STATE_BTN_SESION, false);
    }

    @Override
    public void onClick(View v) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // Si hay conexión a Internet en este momento
            sesionAbierta();
            iniciarSesion();
        } else {
            // No hay conexión a Internet en este momento
            sinConexion();
        }
    }

    private void iniciarSesion(){
        String usuario = Objects.requireNonNull(UsuarioTextInputEditText.getText()).toString().trim();
        String contrasena = Objects.requireNonNull(PasswordTextInputEditText.getText()).toString().trim();

        UsuarioTextInputLayout.setError(null);
        PasswordTextInputLayout.setError(null);

        if (TextUtils.isEmpty(usuario)) { UsuarioTextInputLayout.setError("Este campo está vacío"); return; }

        if (TextUtils.isEmpty(contrasena)) { PasswordTextInputLayout.setError("Este campo está vacío"); return; }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        obtenerDatos(usuario,contrasena);
    }

    private void obtenerDatos(String email, String pass) {

        ProgressDialog = new ProgressDialog(this);
        ProgressDialog.setTitle("Cargando");
        ProgressDialog.setMessage("Validando credenciales...");
        ProgressDialog.setCancelable(false);
        ProgressDialog.show();

        TruckApiService service = retrofit.create(TruckApiService.class);

        /* Probando */
        Call<Token> truckRespuestaCallToken = service.obtenerToken(new LoginDatos(email,pass));
        truckRespuestaCallToken.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                if (response.isSuccessful()) {
                    Token token = response.body();
                    assert token != null;
                    String success = token.getStatus();
                    if (success.equals("success")) {
                        guardarEstadoSesion();
                        guardarPreferencias(email, pass);
                        //Toast.makeText(Login.this, "Bienvenido: " + listaUsuario.get(0).getEmail(), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Login.this, BandejaEntrada.class);
                        startActivity(intent);
                        ProgressDialog.dismiss();

                    }else{
                        UsuarioTextInputLayout.setError("Revise los campos");
                        PasswordTextInputLayout.setError("Revise los campos");
                        ProgressDialog.dismiss();
                    }
                } else {
                    Log.e(TAG, " onResponse: " + response.errorBody());
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);snackbar.show();
                    ProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                Log.e(TAG, " onFailure: " + t.getMessage());
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);snackbar.show();
                ProgressDialog.dismiss();
            }
        });
    }

    private void guardarPreferencias(String email, String pass) {
        SharedPreferences preferences=getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("email", email);
        editor.putString("pass", pass);
        editor.apply();
    }

    public void sinConexion(){
        AlertDialog.Builder alerta = new AlertDialog.Builder(Login.this);
        alerta  .setCancelable(false).setPositiveButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
        AlertDialog titulo = alerta.create();
        titulo.setTitle("No tiene conexión...");
        titulo.show();
    }

    public void cerrarTeclado(View view) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
