package com.example.truckspain.entidades;

public class LoginDatos {
    private String email;
    private String pass;

    public LoginDatos(String email, String password) {
        this.email = email;
        this.pass = password;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The password
     */
    public String getPassword() {
        return pass;
    }

    /**
     *
     * @param password
     * The password
     */
    public void setPassword(String password) {
        this.pass = password;
    }

}
