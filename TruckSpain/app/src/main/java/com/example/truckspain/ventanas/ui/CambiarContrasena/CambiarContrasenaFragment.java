package com.example.truckspain.ventanas.ui.CambiarContrasena;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.truckspain.R;
import com.example.truckspain.api.TruckApiService;
import com.example.truckspain.entidades.TrabajosUpdateDatos;
import com.example.truckspain.entidades.UsuariosUpdateDatos;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class CambiarContrasenaFragment extends Fragment {

    private View alertLayout;
    private TextInputEditText ActualPasswordTextInputEditText, NuevaPasswordTextInputEditText, RepetirPasswordTextInputEditText;
    private TextInputLayout ActualPasswordTextInputLayout, NuevaPasswordTextInputLayout, RepetirPasswordTextInputLayout;
    private ProgressDialog ProgressDialog;

    @SuppressLint("InflateParams")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_cambiarcontrasena, container, false);
        alertLayout = inflater.inflate(R.layout.layout_alert_cambiarcontrasena, null);

        ActualPasswordTextInputEditText = root.findViewById(R.id.ActualPasswordTextInputEditText);
        NuevaPasswordTextInputEditText = root.findViewById(R.id.NuevaPasswordTextInputEditText);
        RepetirPasswordTextInputEditText = root.findViewById(R.id.RepetirPasswordTextInputEditText);

        ActualPasswordTextInputLayout = root.findViewById(R.id.ActualPasswordTextInputLayout);
        NuevaPasswordTextInputLayout = root.findViewById(R.id.NuevaPasswordTextInputLayout);
        RepetirPasswordTextInputLayout = root.findViewById(R.id.RepetirPasswordTextInputLayout);

        Button GuardarCambiosButton = root.findViewById(R.id.GuardarCambiosButton);
        GuardarCambiosButton.setOnClickListener(v -> ComprobarCamposValidos());

        return root;
    }

    private void ComprobarCamposValidos() {
        ConnectivityManager connectivityManager = (ConnectivityManager) requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            SharedPreferences preferences = this.requireActivity().getSharedPreferences("credenciales", Context.MODE_PRIVATE);
            String email_usuario = preferences.getString("email", "No existe");
            String contrasena_usuario = preferences.getString("pass", "No existe");

            System.out.println(email_usuario);
            System.out.println(contrasena_usuario);

            String contrasenaactual = Objects.requireNonNull(ActualPasswordTextInputEditText.getText()).toString().trim();
            String contrasenanueva = Objects.requireNonNull(NuevaPasswordTextInputEditText.getText()).toString().trim();
            String contrasenarepetir = Objects.requireNonNull(RepetirPasswordTextInputEditText.getText()).toString().trim();
            String contrasenaactualMD5 = convertToMd5(contrasenaactual);

            ActualPasswordTextInputLayout.setError(null);
            NuevaPasswordTextInputLayout.setError(null);
            RepetirPasswordTextInputLayout.setError(null);

            if (TextUtils.isEmpty(contrasenaactual) || !contrasenaactual.equals(contrasena_usuario)) {
                ActualPasswordTextInputLayout.setError("Campo Inválido");
                return;
            }
            if (TextUtils.isEmpty(contrasenanueva) || contrasenanueva.length() < 7) {
                NuevaPasswordTextInputLayout.setError("Campo Inválido");
                return;
            }
            if (TextUtils.isEmpty(contrasenarepetir) || !contrasenanueva.equals(contrasenarepetir)) {
                RepetirPasswordTextInputLayout.setError("Campo Inválido");
                return;
            }

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            updateContrasenaUsuario(email_usuario,contrasenarepetir);
        }else{sinConexion();}
    }

    private void cambiarContrasena() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(requireActivity());
        alertDialog.setCancelable(false)
                   .setPositiveButton("Aceptar", (dialogInterface, i) -> getActivity().finish());
        AlertDialog alertDialogLayout = alertDialog.create();
        alertDialogLayout.setView(alertLayout);
        alertDialogLayout.show();
    }

    private void updateContrasenaUsuario(String email, String pass) {

        ProgressDialog = new ProgressDialog(getActivity());
        ProgressDialog.setTitle("Cargando");
        ProgressDialog.setMessage("Cambiando contraseña...");
        ProgressDialog.setCancelable(false);
        ProgressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://truckspain.000webhostapp.com/apitruck/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TruckApiService service = retrofit.create(TruckApiService.class);

        Call<String> truckRespuestaCallUpdateUsuarios = service.updateUsuario(new UsuariosUpdateDatos(email, pass));
        truckRespuestaCallUpdateUsuarios.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.isSuccessful()) {
                    String usuario = response.body();
                    assert usuario != null;
                    ProgressDialog.dismiss();
                    cambiarContrasena();

                } else {
                    Log.e(TAG, " onResponse: " + response.errorBody());
                    Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    ProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Log.e(TAG, " onFailure: " + t.getMessage());
                Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                snackbar.show();
                ProgressDialog.dismiss();
            }
        });
    }

    private static String convertToMd5(final String md5) {
        StringBuffer sb=null;
        try {
            final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(md5.getBytes(StandardCharsets.UTF_8));
            sb = new StringBuffer();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (final java.security.NoSuchAlgorithmException ignored) {
        }
        assert sb != null;
        return sb.toString();
    }

    private void sinConexion(){
        AlertDialog.Builder alerta = new AlertDialog.Builder(requireActivity());
        alerta  .setCancelable(false).setPositiveButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
        AlertDialog titulo = alerta.create();
        titulo.setTitle("No tiene conexión...");
        titulo.show();
    }
}
