package com.example.truckspain.ventanas;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.truckspain.R;
import com.example.truckspain.api.TruckApiService;
import com.example.truckspain.clasesauxiliares.Usuario;
import com.example.truckspain.entidades.TrabajosUpdateDatos;
import com.example.truckspain.entidades.UsuariosDatos;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class InformacionAlbaran extends AppCompatActivity {

    private LocationManager locationManagerFinal;
    private String id_trabajo, nombre, estado, nifempresa, lugar_carga, obra, fecha_realizar, email, peso, observacion;
    private String nombre_empresa, direccion, poblacion, codigo_postal, telefono, email_empresa;
    private ProgressDialog ProgressDialog;
    private TextView rellenarNombreTransportistaTextView;
    String DIRECTORYUSER = Environment.getExternalStorageDirectory().getPath() + "/Truck/user/";
    String StoredPathUser = DIRECTORYUSER + "user_sign" + ".png";

    String DIRECTORYCLIENTE = Environment.getExternalStorageDirectory().getPath() + "/Truck/sign/";
    String nombreClienteFirma = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    String StoredPathCliente = DIRECTORYCLIENTE + nombreClienteFirma+ "-clien" + ".png";

    String nombreResponsableCliente, emailResponsableCliente;

    ImageView FirmaUsuarioImageView;
    Button Btn_FIRMAUSUARIO;
    Button BorrarFirmaButton;
    Dialog dialog;
    LinearLayout linearLayout;
    Button mGetSign;
    Signature signature;

    private TextInputEditText NombreResponsableTextInputEditText, EmailClienteTextInputEditText;
    private TextInputLayout NombreResponsableTextInputLayout, EmailClienteTextInputLayout;

    @SuppressLint({"SetTextI18n", "SourceLockedOrientationActivity"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_informacionalbaran);

        FirmaUsuarioImageView = findViewById(R.id.FirmaUsuarioImageView);
        Btn_FIRMAUSUARIO = findViewById(R.id.Btn_FIRMAUSUARIO);
        BorrarFirmaButton = findViewById(R.id.BorrarFirmaButton);

        NombreResponsableTextInputEditText = findViewById(R.id.NombreResponsableTextInputEditText);
        EmailClienteTextInputEditText = findViewById(R.id.EmailClienteTextInputEditText);
        NombreResponsableTextInputLayout = findViewById(R.id.NombreResponsableTextInputLayout);
        EmailClienteTextInputLayout = findViewById(R.id.EmailClienteTextInputLayout);
        NombreResponsableTextInputEditText.setText(""); EmailClienteTextInputEditText.setText("");

        /* Datos activity anterior */
        Intent intent = getIntent();
        id_trabajo = intent.getStringExtra("id_trabajo");            nombre = intent.getStringExtra("nombre");
        fecha_realizar = intent.getStringExtra("fecha_realizar");    estado = intent.getStringExtra("estado");
        email = intent.getStringExtra("email");                      nifempresa = intent.getStringExtra("nifempresa");
        peso = intent.getStringExtra("peso");                        lugar_carga = intent.getStringExtra("lugar_carga");
        observacion = intent.getStringExtra("observacion");          obra = intent.getStringExtra("obra");

        nombre_empresa = intent.getStringExtra("nombre_empresa");    codigo_postal = intent.getStringExtra("codigo_postal");
        direccion = intent.getStringExtra("direccion");              telefono = intent.getStringExtra("telefono");
        poblacion = intent.getStringExtra("poblacion");              email_empresa = intent.getStringExtra("email_empresa");

        TextView rellenarNumeroAlbaranTextView = findViewById(R.id.RellenarNumeroAlbaranTextView); rellenarNumeroAlbaranTextView.setText("Nº Albarán: "+id_trabajo);
        rellenarNombreTransportistaTextView = findViewById(R.id.RellenarNombreTransportistaTextView);
        locationManagerFinal = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        obtenerDatos(email);

        ImageView rellenarFirmaTransportistaImageView = findViewById(R.id.rellenarFirmaTransportistaImageView);
        rellenarFirmaTransportistaImageView.setImageBitmap(BitmapFactory.decodeFile(StoredPathUser));

        /* Configurar y guardar firma de cliente */
        File Directory = new File(DIRECTORYCLIENTE);
        if (!Directory.exists()) {
            Directory.mkdir();
        }

        File file = new File(StoredPathCliente);
        if(file.exists()){
            FirmaUsuarioImageView.setImageBitmap(BitmapFactory.decodeFile(StoredPathCliente));
            Btn_FIRMAUSUARIO.setVisibility(View.INVISIBLE);
            BorrarFirmaButton.setVisibility(View.VISIBLE);
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_configurarfirma_firmar);
        dialog.setCancelable(true);
        Btn_FIRMAUSUARIO.setOnClickListener(v -> dialog_action());

        BorrarFirmaButton.setOnClickListener(v -> {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setCancelable(false)
                    .setPositiveButton("Aceptar", (dialogInterface, i) -> aceptarBorrarFirma())
                    .setNegativeButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
            AlertDialog alertDialogLayout = alertDialog.create();
            alertDialogLayout.setTitle("¿Quieres borrar tú firma?");
            alertDialogLayout.setMessage("Recuerda: Si no tienes establecida una firma, no podrás realizar trabajos.");
            alertDialogLayout.show();
        });
    }

    public void volverActivityAnterior(View view) {
        finish();
    }

    private void obtenerDatos(String email) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            ProgressDialog = new ProgressDialog(this);
            ProgressDialog.setTitle("Cargando");
            ProgressDialog.setMessage("Recogiendo datos...");
            ProgressDialog.setCancelable(false);
            ProgressDialog.show();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://truckspain.000webhostapp.com/apitruck/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            TruckApiService service = retrofit.create(TruckApiService.class);

            Call<List<Usuario>> truckRespuestaCallUsuario = service.obtenerUsuario(new UsuariosDatos(email));
            truckRespuestaCallUsuario.enqueue(new Callback<List<Usuario>>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<List<Usuario>> call, @NonNull Response<List<Usuario>> response) {
                    if (response.isSuccessful()) {
                        List<Usuario> usuario = response.body();
                        assert usuario != null;

                        rellenarNombreTransportistaTextView.setText(usuario.get(0).getNombre_usuario()+" "+ usuario.get(0).getApellidos_usuario());
                        ProgressDialog.dismiss();

                    } else {
                        Log.e(TAG, " onResponse: " + response.errorBody());
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                        ProgressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<List<Usuario>> call, @NonNull Throwable t) {
                    Log.e(TAG, " onFailure: " + t.getMessage());
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    ProgressDialog.dismiss();
                }
            });

        } else { sinConexion(); }
    }

    @SuppressLint("MissingPermission")
    public void enviarEmail(View view) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            nombreResponsableCliente = Objects.requireNonNull(NombreResponsableTextInputEditText.getText()).toString().trim();
            emailResponsableCliente = Objects.requireNonNull(EmailClienteTextInputEditText.getText()).toString().trim();

            NombreResponsableTextInputLayout.setError(null);
            EmailClienteTextInputLayout.setError(null);

            if (TextUtils.isEmpty(nombreResponsableCliente)) { NombreResponsableTextInputLayout.setError("Este campo está vacío"); return; }

            if (TextUtils.isEmpty(emailResponsableCliente)) { EmailClienteTextInputLayout.setError("Este campo está vacío"); return; }

            if (!BorrarFirmaButton.isShown()){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setCancelable(false)
                        .setPositiveButton("Aceptar", (dialogInterface, i) ->dialogInterface.dismiss());
                AlertDialog alertDialogLayout = alertDialog.create();
                alertDialogLayout.setTitle("Atención");
                alertDialogLayout.setMessage("El responsable necesita firmar para poder enviar el albarán.");
                alertDialogLayout.show();
                return;
            }

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setCancelable(false)
                    .setPositiveButton("Aceptar", (dialogInterface, i) -> {
                        ProgressDialog = new ProgressDialog(this);
                        ProgressDialog.setTitle("Cargando");
                        ProgressDialog.setMessage("Enviando e-mail...");
                        ProgressDialog.setCancelable(false);
                        ProgressDialog.show();
                        locationManagerFinal.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 20 * 1000, 10, locationListenerNetworkFinal);
                    })
                    .setNegativeButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
            AlertDialog alertDialogLayout = alertDialog.create();
            alertDialogLayout.setTitle("¿Quieres enviar el Albarán?");
            alertDialogLayout.setMessage("Una vez que lo hayas enviado, no lo podrás enviar otra vez.");
            alertDialogLayout.show();

        } else { sinConexion(); }
    }

    private void updateTrabajo(String id_trabajo, String hora_terminado, String ubicacionFinal) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://truckspain.000webhostapp.com/apitruck/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            TruckApiService service = retrofit.create(TruckApiService.class);

            Call<String> truckRespuestaCallUpdateTrabajos = service.updateTrabajo(new TrabajosUpdateDatos(id_trabajo, "", "", hora_terminado,ubicacionFinal, Objects.requireNonNull(NombreResponsableTextInputEditText.getText()).toString().trim()));
            truckRespuestaCallUpdateTrabajos.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    if (response.isSuccessful()) {
                        String trabajo = response.body();
                        assert trabajo != null;
                        generarAlbaran();

                        ProgressDialog.dismiss();
                        truckRespuestaCallUpdateTrabajos.cancel();
                    } else {
                        Log.e(TAG, " onResponse: " + response.errorBody());
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                        ProgressDialog.dismiss();
                        truckRespuestaCallUpdateTrabajos.cancel();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    Log.e(TAG, " onFailure: " + t.getMessage());
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    ProgressDialog.dismiss();
                    truckRespuestaCallUpdateTrabajos.cancel();
                }
            });
        } else { sinConexion(); }
    }

    public void generarAlbaran() {
        Intent intent = new Intent(InformacionAlbaran.this, GenerarAlbaran.class);

        intent.putExtra("id_trabajo",id_trabajo);
        intent.putExtra("fecha", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date()));
        intent.putExtra("nombre_empresa",nombre_empresa);
        intent.putExtra("nifempresa",nifempresa);
        intent.putExtra("email_empresa",email_empresa);
        intent.putExtra("telefono",telefono);
        intent.putExtra("direccion",direccion);
        intent.putExtra("poblacion",poblacion);
        intent.putExtra("codigo_postal",codigo_postal);
        intent.putExtra("nombre",nombre);
        intent.putExtra("peso",peso);
        intent.putExtra("nombreTransportista",rellenarNombreTransportistaTextView.getText());
        intent.putExtra("nombreResponsableCliente", nombreResponsableCliente);
        intent.putExtra("emailResponsableCliente", emailResponsableCliente);
        intent.putExtra("rutaFirmaCliente", StoredPathCliente);

        startActivity(intent);
        finish();
    }

    private void aceptarBorrarFirma() {
        FirmaUsuarioImageView.setImageResource(R.color.white);
        Btn_FIRMAUSUARIO.setVisibility(View.VISIBLE);
        BorrarFirmaButton.setVisibility(View.INVISIBLE);
    }

    private void dialog_action() {
        linearLayout = dialog.findViewById(R.id.linearLayout);
        signature = new Signature(getApplicationContext(), null);
        signature.setBackgroundColor(Color.WHITE);

        linearLayout.addView(signature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        Button mClear = dialog.findViewById(R.id.clear);
        mGetSign = dialog.findViewById(R.id.getsign);
        mGetSign.setEnabled(false);
        View view = linearLayout;

        mClear.setOnClickListener(v -> {
            //BORRAR
            signature.clear();
            mGetSign.setEnabled(false);
        });
        mGetSign.setOnClickListener(v -> {
            //GUARDAR
            FirmaUsuarioImageView.setVisibility(View.VISIBLE);
            Btn_FIRMAUSUARIO.setVisibility(View.INVISIBLE);
            BorrarFirmaButton.setVisibility(View.VISIBLE);

            view.setDrawingCacheEnabled(true);
            signature.save(view, StoredPathCliente);
            dialog.dismiss();

            FirmaUsuarioImageView.setImageBitmap(BitmapFactory.decodeFile(""+StoredPathCliente));
        });
        dialog.show();
    }

    public class Signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        @SuppressLint("WrongThread")
        public void save(View v, String StoredPath) {

            Bitmap bitmap = Bitmap.createBitmap(linearLayout.getWidth(), linearLayout.getHeight(), Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(bitmap);
            try {
                FileOutputStream mFileOutStream = new FileOutputStream(StoredPath);
                v.draw(canvas);

                bitmap.compress(Bitmap.CompressFormat.PNG, 70, mFileOutStream);

                mFileOutStream.flush();
                mFileOutStream.close();
            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:
                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;
                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {
            Log.v("log_tag", string);
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    private final LocationListener locationListenerNetworkFinal = new LocationListener() {

        public void onLocationChanged(Location location) {
            double longitudeNetwork = location.getLongitude();
            double latitudeNetwork = location.getLatitude();

            runOnUiThread(() -> {
                String Latitude = latitudeNetwork + "";
                String Longitude = longitudeNetwork + "";
                String ubicacionFinal = Latitude + ","+ Longitude;
                String hora_terminado = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
                locationManagerFinal.removeUpdates(locationListenerNetworkFinal);
                updateTrabajo(id_trabajo, hora_terminado, ubicacionFinal);
            });
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }

        @Override
        public void onProviderEnabled(String provider) { }

        @Override
        public void onProviderDisabled(String provider) { }
    };

    public void sinConexion(){
        AlertDialog.Builder alerta = new AlertDialog.Builder(InformacionAlbaran.this);
        alerta  .setCancelable(false).setPositiveButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
        AlertDialog titulo = alerta.create();
        titulo.setTitle("No tiene conexión...");
        titulo.show();
    }
}
