package com.example.truckspain.ventanas;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.truckspain.R;
import com.example.truckspain.clasesauxiliares.JavaMailApi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class GenerarAlbaran extends AppCompatActivity {

    private LinearLayout LinearLayout;
    private Bitmap bitmap;

    Integer emails = 0;
    String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/Truck/user/";
    String DIRECTORYPDF = Environment.getExternalStorageDirectory().getPath() + "/Truck/PDF/";
    String StoredPath = DIRECTORY + "user_sign" + ".png";
    String rutaFirmaCliente;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.layout_albaran);

        Intent intent = getIntent();
        String id_trabajo = intent.getStringExtra("id_trabajo");                   String telefono = intent.getStringExtra("telefono");
        String fecha = intent.getStringExtra("fecha");                             String direccion = intent.getStringExtra("direccion");
        String nombre_empresa = intent.getStringExtra("nombre_empresa");           String poblacion = intent.getStringExtra("poblacion");
        String nifempresa = intent.getStringExtra("nifempresa");                   String codigo_postal = intent.getStringExtra("codigo_postal");
        String email_empresa = intent.getStringExtra("email_empresa");             String nombre = intent.getStringExtra("nombre");
        String peso = intent.getStringExtra("peso");                               String nombreTransportista = intent.getStringExtra("nombreTransportista");
        String emailResponsableCliente = intent.getStringExtra("emailResponsableCliente"); String nombreResponsableCliente = intent.getStringExtra("nombreResponsableCliente");
        rutaFirmaCliente = intent.getStringExtra("rutaFirmaCliente");

        String nombrePDF = new SimpleDateFormat("dd-MM-yyyy_HHmmss", Locale.getDefault()).format(new Date());
        String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/Truck/PDF/";
        File file = new File(DIRECTORY);
        if (!file.exists()) {
            file.mkdir();
        }

        ImageView rellenarFirmaTransportistaConclusionesImageView = findViewById(R.id.RellenarFirmaTransportistaConclusionesImageView);
        rellenarFirmaTransportistaConclusionesImageView.setImageBitmap(BitmapFactory.decodeFile(StoredPath));

        ImageView rellenarFirmaClienteConclusionesImageView = findViewById(R.id.RellenarFirmaClienteConclusionesImageView);
        rellenarFirmaClienteConclusionesImageView.setImageBitmap(BitmapFactory.decodeFile(rutaFirmaCliente));

        rellenarCamposAlbaran(id_trabajo, fecha, nombre_empresa, nifempresa, email_empresa, peso, emailResponsableCliente, nombreResponsableCliente, telefono, direccion, poblacion, codigo_postal, nombre, nombreTransportista);

        LinearLayout = findViewById(R.id.LinearLayout);
        final ViewTreeObserver viewTreeObserver = LinearLayout.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(() -> generarPDFAlbaran(id_trabajo, emailResponsableCliente, nombrePDF, DIRECTORY));
    }

    private void generarPDFAlbaran(String id_trabajo, String emailResponsableCliente, String nombrePDF, String DIRECTORY) {
        bitmap = loadBitmapFromView(LinearLayout, LinearLayout.getWidth(), LinearLayout.getHeight());

        crearPDFAlbaran(nombrePDF, DIRECTORY);
        emailPdf(id_trabajo, emailResponsableCliente , nombrePDF);

        /*File firmaCliente = new File(rutaFirmaCliente);
        if(firmaCliente.exists()){
            firmaCliente.delete();
        }*/
        Intent intent = new Intent(GenerarAlbaran.this, InformacionEmail.class);
        startActivity(intent);
        finish();
    }

    private void emailPdf(String id_trabajo, String emailResponsableCliente, String nombrePDF) {
        if(emails==0) {
            String nombrePDFPath = DIRECTORYPDF + nombrePDF + ".pdf";
            String dia = new SimpleDateFormat("dd", Locale.getDefault()).format(new Date());
            String mes = new SimpleDateFormat("MM", Locale.getDefault()).format(new Date());
            String anio = new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date());

            String message = "Hola,\n\nAdjunto el Albarán del pedido Nº " + id_trabajo + " realizado el día " + dia + "/" + mes + "/" + anio + ".\n\nAtentamente, Truck. Spain";
            String subject = "Albarán del Pedido Nº " + id_trabajo + " - " + dia + "/" + mes + "/" + anio;

            JavaMailApi javaMailAPI = new JavaMailApi(this, emailResponsableCliente, subject, message, nombrePDFPath, "truckSpain_"+nombrePDF);
            javaMailAPI.execute();

            emails++;
        }
    }

    private void crearPDFAlbaran(String nombrePDF, String DIRECTORY){
        String StoredPath = DIRECTORY + nombrePDF + ".pdf";

        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        int convertHighet = 3508, convertWidth = 2480;

        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);

        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        canvas.drawPaint(paint);

        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0 , null);
        pdfDocument.finishPage(page);

        File filePath;
        filePath = new File(StoredPath);
        try {
            pdfDocument.writeTo(new FileOutputStream(filePath));
            pdfDocument.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String DIRECTORYSIGN = Environment.getExternalStorageDirectory().getPath() + "/Truck/sign/";
        File dir = new File(""+DIRECTORYSIGN);
        if (dir.isDirectory())
        {
            String[] children = dir.list();
            assert children != null;
            for (String child : children) {
                new File(dir, child).delete();
            }
        }

        String DIRECTORYPDF = Environment.getExternalStorageDirectory().getPath() + "/Truck/PDF/";
        File file = new File(DIRECTORYPDF);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    public static Bitmap loadBitmapFromView(View view, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    @SuppressLint("SetTextI18n")
    private void rellenarCamposAlbaran(String id_trabajo, String fecha, String nombre_empresa, String nifempresa, String email_empresa, String peso, String emailResponsableCliente, String nombreResponsableCliente, String telefono, String direccion, String poblacion, String codigo_postal, String nombre, String nombreTransportista) {
        TextView tituloAlbaranTextView = findViewById(R.id.TituloAlbaranTextView);                          tituloAlbaranTextView.setText("Nº Albarán: "+ id_trabajo);
        TextView rellenarFechaTextView = findViewById(R.id.RellenarFechaTextView);                          rellenarFechaTextView.setText(fecha);
        TextView rellenarClienteTextView = findViewById(R.id.RellenarClienteTextView);                      rellenarClienteTextView.setText(nombre_empresa);
        TextView rellenarNIFTextView = findViewById(R.id.RellenarNIFTextView);                              rellenarNIFTextView.setText(nifempresa);
        TextView rellenarEmailTextView = findViewById(R.id.RellenarEmailTextView);                          rellenarEmailTextView.setText(email_empresa);
        TextView rellenarTelefonoTextView = findViewById(R.id.RellenarTelefonoTextView);                    rellenarTelefonoTextView.setText(telefono);
        TextView rellenarDireccionTextView = findViewById(R.id.RellenarDireccionTextView);                  rellenarDireccionTextView.setText(direccion);
        TextView rellenarPoblacionTextView = findViewById(R.id.RellenarPoblacionTextView);                  rellenarPoblacionTextView.setText(poblacion);
        TextView rellenarCodigoPostalTextView = findViewById(R.id.RellenarCodigoPostalTextView);            rellenarCodigoPostalTextView.setText(codigo_postal);
        TextView rellenarNombreMercanciaTextView = findViewById(R.id.RellenarNombreMercanciaTextView);      rellenarNombreMercanciaTextView.setText(nombre);
        TextView rellenarPesoMercanciaTextView = findViewById(R.id.RellenarPesoMercanciaTextView);          rellenarPesoMercanciaTextView.setText(peso+" t");
        TextView rellenarNombreResponsableClienteConclusionesTextView = findViewById(R.id.RellenarNombreResponsableClienteConclusionesTextView); rellenarNombreResponsableClienteConclusionesTextView.setText(nombreResponsableCliente);
        TextView rellenarNombreTransportistaConclusionesTextView = findViewById(R.id.RellenarNombreTransportistaConclusionesTextView); rellenarNombreTransportistaConclusionesTextView.setText(nombreTransportista);
    }
}
