package com.example.truckspain.entidades;

public class TrabajosUpdateDatos {
    private String id_trabajo;
    private String fecha_inicio;
    private String ubicacion_inicio;
    private String fecha_final;
    private String ubicacion_final;
    private String nombre_responsable;

    public TrabajosUpdateDatos(String id_trabajo, String fecha_inicio, String ubicacion_inicio, String fecha_final, String ubicacion_final, String nombre_responsable) {
        this.id_trabajo = id_trabajo;
        this.fecha_inicio = fecha_inicio;
        this.ubicacion_inicio = ubicacion_inicio;
        this.fecha_final = fecha_final;
        this.ubicacion_final = ubicacion_final;
        this.nombre_responsable = nombre_responsable;
    }

    /**
     *
     * @return
     * The id_trabajo
     */
    public String getId_trabajo() {
            return id_trabajo;
        }

    /**
     *
     * @param id_trabajo
     * The id_trabajo
     */
    public void setId_trabajo(String id_trabajo) {
            this.id_trabajo = id_trabajo;
        }

    /**
     *
     * @return
     * The fecha_inicio
     */
    public String getFecha_inicio() {
        return fecha_inicio;
    }

    /**
     *
     * @param fecha_inicio
     * The id_trabajo
     */
    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    /**
     *
     * @return
     * The ubicacion_inicio
     */
    public String getUbicacion_inicio() {
        return ubicacion_inicio;
    }

    /**
     *
     * @param ubicacion_inicio
     * The ubicacion_inicio
     */
    public void setUbicacion_inicio(String ubicacion_inicio) {
        this.ubicacion_inicio = ubicacion_inicio;
    }

    /**
     *
     * @return
     * The fecha_final
     */
    public String getFecha_final() {
        return fecha_final;
    }

    /**
     *
     * @param fecha_final
     * The fecha_final
     */
    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }

    /**
     *
     * @return
     * The ubicacion_final
     */
    public String getUbicacion_final() {
        return ubicacion_final;
    }

    /**
     *
     * @param ubicacion_final
     * The ubicacion_final
     */
    public void setUbicacion_final(String ubicacion_final) {
        this.ubicacion_final = ubicacion_final;
    }

    /**
     *
     * @return
     * The nombre_responsable
     */
    public String getNombre_responsable() {
        return nombre_responsable;
    }

    /**
     *
     * @param nombre_responsable
     * The nombre_responsable
     */
    public void setNombre_responsable(String nombre_responsable) {
        this.nombre_responsable = nombre_responsable;
    }

}
