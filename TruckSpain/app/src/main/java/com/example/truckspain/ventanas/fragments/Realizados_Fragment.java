package com.example.truckspain.ventanas.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.truckspain.R;
import com.example.truckspain.api.TruckApiService;
import com.example.truckspain.clasesauxiliares.AdaptadorRecycler;
import com.example.truckspain.clasesauxiliares.Trabajo;
import com.example.truckspain.entidades.TrabajosDatos;
import com.example.truckspain.ventanas.BandejaEntrada;
import com.example.truckspain.ventanas.InformacionPendientes;
import com.example.truckspain.ventanas.InformacionRealizados;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Realizados_Fragment extends Fragment {

    private android.app.ProgressDialog ProgressDialog;
    private RecyclerView recyclerView;
    private ArrayList<Trabajo> listTrabajo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_realizados, container, false);
        recyclerView = rootView.findViewById(R.id.TrabajosRealizadosRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        BandejaEntrada bandejaEntrada = (BandejaEntrada) getActivity();
        String email_usuario = bandejaEntrada.getEmailUsuario();
        ObtenerDatos(email_usuario);

        return rootView;
    }

    private void ObtenerDatos(String email) {

        ProgressDialog = new ProgressDialog(getActivity());
        ProgressDialog.setTitle("Cargando");
        ProgressDialog.setMessage("Recogiendo datos...");
        ProgressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        ProgressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://truckspain.000webhostapp.com/apitruck/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TruckApiService service = retrofit.create(TruckApiService.class);

        Call<List<Trabajo>> truckRespuestaCallTrabajos = service.obtenerTrabajos(new TrabajosDatos(email));
        truckRespuestaCallTrabajos.enqueue(new Callback<List<Trabajo>>() {
            @Override
            public void onResponse(@NonNull Call<List<Trabajo>> call, @NonNull Response<List<Trabajo>> response) {
                if (response.isSuccessful()) {
                    List<Trabajo> trabajo = response.body();
                    assert trabajo != null;

                    listTrabajo = new ArrayList<>();
                    for (int i = 0; i < trabajo.size(); i++) {
                        if(trabajo.get(i).getEstado().equals("1")){
                            listTrabajo.add(new Trabajo("" + trabajo.get(i).getId_trabajo(), "" + trabajo.get(i).getNombre(), "" + trabajo.get(i).getFecha_realizar(), "Realizado", ""+trabajo.get(i).getEmail(), ""+trabajo.get(i).getNifempresa(), "", ""+trabajo.get(i).getPeso(), ""+trabajo.get(i).getLugar_carga(), ""+trabajo.get(i).getFecha_inicio(), ""+trabajo.get(i).getFecha_final(), "", "", ""+trabajo.get(i).getNombre_responsable(), "", ""+trabajo.get(i).getObservacion(), ""+trabajo.get(i).getObra()));
                        }
                    }
                    AdaptadorRecycler adaptadorRecycler = new AdaptadorRecycler(listTrabajo);
                    adaptadorRecycler.setOnClickListener(view -> {
                        //Código de ejecución del trabajo
                        Intent intent = new Intent(getActivity(), InformacionRealizados.class);
                        intent.putExtra("id_trabajo", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getId_trabajo());
                        intent.putExtra("nombre", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getNombre());
                        intent.putExtra("fecha_realizar", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getFecha_realizar());
                        intent.putExtra("estado", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getEstado());
                        intent.putExtra("email", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getEmail());
                        intent.putExtra("nifempresa", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getNifempresa());
                        intent.putExtra("peso", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getPeso());
                        intent.putExtra("lugar_carga", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getLugar_carga());
                        intent.putExtra("fecha_inicio", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getFecha_inicio());
                        intent.putExtra("fecha_final", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getFecha_final());
                        intent.putExtra("nombre_responsable", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getNombre_responsable());
                        intent.putExtra("observacion", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getObservacion());
                        intent.putExtra("obra", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getObra());
                        startActivity(intent);
                        getActivity().finish();
                    });

                    recyclerView.setAdapter(adaptadorRecycler);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    System.out.println("truckRespuestaCallTrabajos");
                    truckRespuestaCallTrabajos.cancel();

                    ProgressDialog.dismiss();


                } else {
                    Log.e(TAG, " onResponse: " + response.errorBody());
                    Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    ProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Trabajo>> call, @NonNull Throwable t) {
                Log.e(TAG, " onFailure: " + t.getMessage());
                Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                snackbar.show();
                ProgressDialog.dismiss();
            }
        });
    }
}
