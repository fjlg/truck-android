package com.example.truckspain.ventanas.ui.BandejaEntrada;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.truckspain.R;
import com.example.truckspain.ventanas.Login;
import com.example.truckspain.ventanas.fragments.Pendientes_Fragment;
import com.example.truckspain.ventanas.fragments.Realizados_Fragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class BandejaEntradaFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        ViewPager viewPager = root.findViewById(R.id.view_pager);
        setupViewPager(viewPager);

        FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(view1 -> cerrarSesion(inflater));

        TabLayout tabs = root.findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        return root;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new Pendientes_Fragment(), "PENDIENTES");
        adapter.addFragment(new Realizados_Fragment(), "REALIZADOS");
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        Adapter(FragmentManager manager) {
            super(manager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @SuppressLint("InflateParams")
    private void cerrarSesion(LayoutInflater inflater) {
        ConnectivityManager connectivityManager = (ConnectivityManager) requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            Login.MantenerSesionRadioButton.setChecked(false);
            SharedPreferences preferences = requireActivity().getSharedPreferences(Login.STRING_PREFERNECES, MODE_PRIVATE);
            preferences.edit().putBoolean(Login.PREFERENCE_STATE_BTN_SESION, Login.MantenerSesionRadioButton.isChecked()).apply();

            View alertLayout = inflater.inflate(R.layout.layout_alert_cerrarsesion, null);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(requireActivity());
            alertDialog.setPositiveButton("Aceptar", (dialogInterface, i) -> requireActivity().finish())
                       .setNegativeButton("Cancelar", (dialogInterface, i) -> dialogInterface.cancel());
            AlertDialog alertDialogLayout = alertDialog.create();
            alertDialogLayout.setView(alertLayout);
            alertDialogLayout.show();
        }else{sinConexion();}
    }

    private void sinConexion(){
        AlertDialog.Builder alerta = new AlertDialog.Builder(requireActivity());
        alerta  .setCancelable(false).setPositiveButton("Cancelar", (dialogInterface, i) -> dialogInterface.dismiss());
        AlertDialog titulo = alerta.create();
        titulo.setTitle("No tiene conexión...");
        titulo.show();
    }
}
