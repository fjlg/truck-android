package com.example.truckspain.clasesauxiliares;

public class Empresa {
    private String nif_empresa;
    private String nombre_empresa;
    private String direccion;
    private String provincia;
    private String poblacion;
    private String codigo_postal;
    private String telefono;
    private String email_empresa;

    public Empresa(String nif_empresa, String nombre_empresa, String direccion, String provincia, String poblacion, String codigo_postal, String telefono, String email_empresa) {
        this.nif_empresa = nif_empresa;
        this.nombre_empresa = nombre_empresa;
        this.direccion = direccion;
        this.provincia = provincia;
        this.poblacion = poblacion;
        this.codigo_postal = codigo_postal;
        this.telefono = telefono;
        this.email_empresa = email_empresa;
    }

    public String getNif_empresa() {
        return nif_empresa;
    }

    public void setNif_empresa(String nif_empresa) {
        this.nif_empresa = nif_empresa;
    }

    public String getNombre_empresa() {
        return nombre_empresa;
    }

    public void setNombre_empresa(String nombre_empresa) {
        this.nombre_empresa = nombre_empresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getCodigo_postal() {
        return codigo_postal;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = codigo_postal;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail_empresa() {
        return email_empresa;
    }

    public void setEmail_empresa(String email_empresa) {
        this.email_empresa = email_empresa;
    }
}
