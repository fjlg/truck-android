package com.example.truckspain.clasesauxiliares;

import android.os.Environment;
import java.io.File;


public class EstructuraDirectorios {

    private String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/Truck";
    public void ComprobarEstructuraDirectorios(){
        File dir = new File(""+DIRECTORY);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            assert children != null;
            for (String child : children) {
                new File(dir, child).delete();
            }
        }

        File file = new File(DIRECTORY);
        if (!file.exists()) {
            file.mkdir();
        }
    }

}
