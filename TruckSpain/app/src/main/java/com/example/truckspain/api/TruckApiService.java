package com.example.truckspain.api;

import com.example.truckspain.clasesauxiliares.Empresa;
import com.example.truckspain.clasesauxiliares.Token;
import com.example.truckspain.clasesauxiliares.Trabajo;
import com.example.truckspain.clasesauxiliares.TruckRespuesta;
import com.example.truckspain.clasesauxiliares.Usuario;
import com.example.truckspain.entidades.EmpresasDatos;
import com.example.truckspain.entidades.LoginDatos;
import com.example.truckspain.entidades.TrabajosDatos;
import com.example.truckspain.entidades.TrabajosUpdateDatos;
import com.example.truckspain.entidades.UsuariosDatos;
import com.example.truckspain.entidades.UsuariosUpdateDatos;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface TruckApiService {

    @HTTP(method = "POST", path = "api/trabajo/usuario", hasBody = true)
    @Headers("Content-Type: application/json")
    Call<List<Trabajo>> obtenerTrabajos(@Body TrabajosDatos body);

    @HTTP(method = "POST", path = "api/empresa/mostrar", hasBody = true)
    @Headers("Content-Type: application/json")
    Call<List<Empresa>> obtenerEmpresa(@Body EmpresasDatos body);

    @HTTP(method = "POST", path = "api/trabajo/actualizar/app", hasBody = true)
    @Headers("Content-Type: application/json")
    Call<String> updateTrabajo(@Body TrabajosUpdateDatos body);

    @HTTP(method = "POST", path = "api/usuario/actualizar/app", hasBody = true)
    @Headers("Content-Type: application/json")
    Call<String> updateUsuario(@Body UsuariosUpdateDatos body);

    @HTTP(method = "POST", path = "api/usuario/mostrar/app", hasBody = true)
    @Headers("Content-Type: application/json")
    Call<List<Usuario>> obtenerUsuario(@Body UsuariosDatos body);

    @HTTP(method = "POST", path = "api/tokens", hasBody = true)
    @Headers("Content-Type: application/json")
    Call<Token> obtenerToken(@Body LoginDatos body);

    /* POST Contraseña
    @POST("truck/contrasena/")
    Call<Void> cambiarContrasena(@Query("user") String user, @Query("pass") String pass);
    */
}
