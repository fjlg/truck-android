package com.example.truckspain.ventanas.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.truckspain.R;
import com.example.truckspain.api.TruckApiService;
import com.example.truckspain.clasesauxiliares.AdaptadorRecycler;
import com.example.truckspain.clasesauxiliares.Trabajo;
import com.example.truckspain.entidades.TrabajosDatos;
import com.example.truckspain.ventanas.BandejaEntrada;
import com.example.truckspain.ventanas.InformacionPendientes;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Pendientes_Fragment extends Fragment {

    private ProgressDialog ProgressDialog;
    private RecyclerView recyclerView;
    private ArrayList<Trabajo> listTrabajo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_pendientes, container, false);
        recyclerView = rootView.findViewById(R.id.TrabajosPendientesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        BandejaEntrada bandejaEntrada = (BandejaEntrada) getActivity();
        String email_usuario = Objects.requireNonNull(bandejaEntrada).getEmailUsuario();
        obtenerDatos(email_usuario);

        return rootView;
    }

    private void obtenerDatos(String email) {
        ProgressDialog = new ProgressDialog(getActivity());
        ProgressDialog.setTitle("Cargando");
        ProgressDialog.setMessage("Recogiendo datos...");
        ProgressDialog.setCancelable(false);
        ProgressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://truckspain.000webhostapp.com/apitruck/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TruckApiService service = retrofit.create(TruckApiService.class);

        Call<List<Trabajo>> truckRespuestaCallTrabajos = service.obtenerTrabajos(new TrabajosDatos(email));
        truckRespuestaCallTrabajos.enqueue(new Callback<List<Trabajo>>() {
            @Override
            public void onResponse(@NonNull Call<List<Trabajo>> call, @NonNull Response<List<Trabajo>> response) {
                if (response.isSuccessful()) {
                    List<Trabajo> trabajo = response.body();
                    assert trabajo != null;

                    listTrabajo = new ArrayList<>();
                    for (int i = 0; i < trabajo.size(); i++) {
                        if(trabajo.get(i).getEstado().equals("0")){
                            listTrabajo.add(new Trabajo(""+trabajo.get(i).getId_trabajo(), ""+trabajo.get(i).getNombre(), ""+trabajo.get(i).getFecha_realizar(), "Pendiente", ""+trabajo.get(i).getEmail(), ""+trabajo.get(i).getNifempresa(), "", ""+trabajo.get(i).getPeso(), ""+trabajo.get(i).getLugar_carga(), "", "", "", "", "", "", ""+trabajo.get(i).getObservacion(), ""+trabajo.get(i).getObra()));
                        }
                        if(trabajo.get(i).getEstado().equals("2")) {
                            listTrabajo.add(new Trabajo("" + trabajo.get(i).getId_trabajo(), "" + trabajo.get(i).getNombre(), "" + trabajo.get(i).getFecha_realizar(), "En Curso", ""+trabajo.get(i).getEmail(), ""+trabajo.get(i).getNifempresa(), "", ""+trabajo.get(i).getPeso(), ""+trabajo.get(i).getLugar_carga(), "", "", "", "", "", "", ""+trabajo.get(i).getObservacion(), ""+trabajo.get(i).getObra()));
                        }
                    }
                    AdaptadorRecycler adaptadorRecycler = new AdaptadorRecycler(listTrabajo);
                    adaptadorRecycler.setOnClickListener(view -> {
                        //Código de ejecución del trabajo
                        Intent intent = new Intent(getActivity(), InformacionPendientes.class);
                        intent.putExtra("id_trabajo", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getId_trabajo());
                        intent.putExtra("nombre", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getNombre());
                        intent.putExtra("fecha_realizar", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getFecha_realizar());
                        intent.putExtra("estado", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getEstado());
                        intent.putExtra("email", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getEmail());
                        intent.putExtra("nifempresa", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getNifempresa());
                        intent.putExtra("peso", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getPeso());
                        intent.putExtra("lugar_carga", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getLugar_carga());
                        intent.putExtra("observacion", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getObservacion());
                        intent.putExtra("obra", listTrabajo.get(recyclerView.getChildAdapterPosition(view)).getObra());
                        startActivity(intent);
                        getActivity().finish();
                    });

                    recyclerView.setAdapter(adaptadorRecycler);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    System.out.println("truckRespuestaCallTrabajos");

                    ProgressDialog.dismiss();

                }
                else {
                    Log.e(TAG, " onResponse: " + response.errorBody());
                    Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    ProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Trabajo>> call, @NonNull Throwable t) {
                Log.e(TAG, " onFailure: " + t.getMessage());
                Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                snackbar.show();
                ProgressDialog.dismiss();
            }
        });
    }
}
