package com.example.truckspain.ventanas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.truckspain.R;
import com.example.truckspain.api.TruckApiService;
import com.example.truckspain.clasesauxiliares.Empresa;
import com.example.truckspain.entidades.EmpresasDatos;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class InformacionRealizados extends AppCompatActivity {

    private ProgressDialog ProgressDialog;

    @SuppressLint({"SetTextI18n", "SourceLockedOrientationActivity"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_informacionrealizados);

        Intent intent = getIntent();
        String id_trabajo = intent.getStringExtra("id_trabajo");            String nombre = intent.getStringExtra("nombre");
        String fecha_realizar = intent.getStringExtra("fecha_realizar");    String nombre_responsable = intent.getStringExtra("nombre_responsable");
        String email = intent.getStringExtra("email");                      String nifempresa = intent.getStringExtra("nifempresa");
        String peso = intent.getStringExtra("peso");                        String lugar_carga = intent.getStringExtra("lugar_carga");
        String obra = intent.getStringExtra("obra");                        String fecha_final = intent.getStringExtra("fecha_final");
        String fecha_inicio = intent.getStringExtra("fecha_inicio");


        TextView rellenarNombreTextView = findViewById(R.id.RellenarNombreTextView);                                        rellenarNombreTextView.setText(nombre);
        TextView rellenarDireccionTextView = findViewById(R.id.RellenarDireccionTextView);                                  rellenarDireccionTextView.setText(lugar_carga);
        TextView rellenarObraTextView = findViewById(R.id.RellenarObraTextView);                                            rellenarObraTextView.setText(obra);
        TextView rellenarFechaMesTextView = findViewById(R.id.RellenarFechaMesTextView);                                    rellenarFechaMesTextView.setText(MesFecha(Objects.requireNonNull(fecha_realizar)));
        TextView rellenarFechaDiaTextView = findViewById(R.id.RellenarFechaDiaTextView);                                    rellenarFechaDiaTextView.setText(fecha_realizar.substring(8,10));
        TextView rellenarFechaAnioTextView = findViewById(R.id.RellenarFechaAnioTextView);                                  rellenarFechaAnioTextView.setText(fecha_realizar.substring(2,4));
        TextView rellenarNumeroAlbaranTextView = findViewById(R.id.RellenarNumeroAlbaranTextView);                          rellenarNumeroAlbaranTextView.setText("Nº Albarán: "+id_trabajo);
        TextView rellenarNombreTransportistaTextView = findViewById(R.id.RellenarNombreTransportistaTextView);              rellenarNombreTransportistaTextView.setText(email);
        TextView rellenarNombreResponsableClienteTextView = findViewById(R.id.RellenarNombreResponsableClienteTextView);    rellenarNombreResponsableClienteTextView.setText(nombre_responsable);
        TextView rellenarFechaEmpezarTrabajoTextView = findViewById(R.id.RellenarFechaEmpezarTrabajoTextView);              rellenarFechaEmpezarTrabajoTextView.setText(Objects.requireNonNull(fecha_inicio).substring(11,16));
        TextView rellenarFechaTerminarTrabajoTextView = findViewById(R.id.RellenarFechaTerminarTrabajoTextView);            rellenarFechaTerminarTrabajoTextView.setText(Objects.requireNonNull(fecha_final).substring(11,16));
        TextView rellenarPesoTextView = findViewById(R.id.RellenarPesoTextView);                                            rellenarPesoTextView.setText(peso+" t");
        TextView rellenarFechaRealizadoTextView = findViewById(R.id.RellenarFechaRealizadoTextView);                        rellenarFechaRealizadoTextView.setText(fecha_final);
        TextView rellenarTiempoTranscurridoTextView = findViewById(R.id.RellenarTiempoTranscurridoTextView);

        try {
            rellenarTiempoTranscurridoTextView.setText(CalcularTiempoTranscurrido(fecha_inicio, fecha_final));
        } catch (ParseException e) {
            rellenarTiempoTranscurridoTextView.setText("");
            e.printStackTrace();
        }

        ObtenerDatos(nifempresa);
    }

    public String MesFecha(String fecha_realizar){
        String mes = fecha_realizar.substring(5,7);
        switch (mes) {
            case "01": return "ENE";
            case "02": return "FEB";
            case "03": return "MAR";
            case "04": return "ABR";
            case "05": return "MAY";
            case "06": return "JUN";
            case "07": return "JUL";
            case "08": return "AGO";
            case "09": return "SEP";
            case "10": return "OCT";
            case "11": return "NOV";
            case "12": return"DIC";
            default: return "ERR";
        }
    }

    private void ObtenerDatos(String nif_empresa) {
        ProgressDialog = new ProgressDialog(this);
        ProgressDialog.setTitle("Cargando");
        ProgressDialog.setMessage("Recogiendo datos...");
        ProgressDialog.setCancelable(false);
        ProgressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://truckspain.000webhostapp.com/apitruck/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TruckApiService service = retrofit.create(TruckApiService.class);

        Call<List<Empresa>> truckRespuestaCallEmpresa = service.obtenerEmpresa(new EmpresasDatos(nif_empresa));
        truckRespuestaCallEmpresa.enqueue(new Callback<List<Empresa>>() {
            @Override
            public void onResponse(@NonNull Call<List<Empresa>> call, @NonNull Response<List<Empresa>> response) {
                if (response.isSuccessful()) {
                    List<Empresa> empresa = response.body();
                    assert empresa != null;

                    TextView rellenarEmpresaTextView = findViewById(R.id.RellenarEmpresaTextView);       rellenarEmpresaTextView.setText(empresa.get(0).getNombre_empresa());
                    TextView rellenarTelefonoTextView = findViewById(R.id.RellenarTelefonoTextView);     rellenarTelefonoTextView.setText(empresa.get(0).getTelefono());
                    ProgressDialog.dismiss();


                } else {
                    Log.e(TAG, " onResponse: " + response.errorBody());
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    ProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Empresa>> call, @NonNull Throwable t) {
                Log.e(TAG, " onFailure: " + t.getMessage());
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Error: Tiempo de espera excedido.", Snackbar.LENGTH_LONG);
                snackbar.show();
                ProgressDialog.dismiss();
            }
        });
    }

    public String CalcularTiempoTranscurrido(String fecha_inicio, String fecha_final) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date fechaInicial=dateFormat.parse(fecha_inicio);
        Date fechaFinal=dateFormat.parse(fecha_final);

        int minutos=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/60000);

        return minutos+" minutos";
    }

    public void volverActivityAnterior(View view) {
        Intent intent = new Intent(InformacionRealizados.this, BandejaEntrada.class);
        startActivity(intent);
        finish();
    }
}
